psql postgres <<SQL
    CREATE USER unleash_user WITH PASSWORD 'password';
    CREATE DATABASE unleash;
    GRANT ALL PRIVILEGES ON DATABASE unleash to unleash_user;
SQL